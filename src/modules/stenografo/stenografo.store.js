import types from './stenografo.store.mutations'

const createStenografoStore = ($http) => {
  return {
    namespaced: true,
    state: {
      isWaiting: false,
      documentList: [],
      document: {
        title: '',
        content: '',
        id: ''
      },
      isSaved: false,
      errors: [],
      documentsCount: null
    },
    getters: {
      isWaiting: (state) => {
        return state.isWaiting
      },
      documentList: (state) => {
        return state.documentList
      },
      document: (state) => {
        return state.document
      },
      isSaved: (state) => {
        return state.isSaved
      },
      errors: (state) => {
        return state.errors
      },
      documentsCount: (state) => {
        return state.documentsCount
      }
    },
    mutations: {
      [types.STENOGRAFO_GET_DOCUMENTS_LIST_REQUEST] (state) {
        state.isWaiting = true
      },
      [types.STENOGRAFO_GET_DOCUMENTS_LIST_SUCCESS] (state, payload) {
        state.isWaiting = false
        state.documentList = payload
      },
      [types.STENOGRAFO_GET_DOCUMENTS_LIST_FAILURE] (state) {
        state.isWaiting = false
      },
      [types.STENOGRAFO_GET_DOCUMENT_REQUEST] (state) {
        state.isWaiting = true
      },
      [types.STENOGRAFO_GET_DOCUMENT_SUCCESS] (state, payload) {
        state.isWaiting = false
        state.document = payload
      },
      [types.STENOGRAFO_GET_DOCUMENT_FAILURE] (state) {
        state.isWaiting = false
      },
      [types.STENOGRAFO_UPDATE_DOCUMENT_CONTENT] (state, payload) {
        state.document.content = payload
      },
      [types.STENOGRAFO_UPDATE_DOCUMENT_TITLE] (state, payload) {
        state.document.title = payload
      },
      [types.STENOGRAFO_UPDATE_DOCUMENT_ID] (state, payload) {
        state.document.id = payload
      },
      [types.STENOGRAFO_SAVE_DOCUMENT_REQUEST] (state) {
        state.isSaved = false
        state.isWaiting = true
      },
      [types.STENOGRAFO_SAVE_DOCUMENT_SUCCESS] (state) {
        state.isSaved = true
        state.isWaiting = false
      },
      [types.STENOGRAFO_SAVE_DOCUMENT_FAILURE] (state, exception) {
        state.isSaved = false
        state.isWaiting = false
        state.errors.push(exception)
      },
      [types.STENOGRAFO_SET_DOCUMENTS_COUNT] (state, documentsCount) {
        state.documentsCount = documentsCount
      }
    },
    actions: {
      async getDocumentList ({ commit, rootGetters }, currentPage) {
        const limit = 10
        const offset = limit * (currentPage - 1)
        commit(types.STENOGRAFO_GET_DOCUMENTS_LIST_REQUEST)
        try {
          const currentRealm = rootGetters['app/currentRealm']
          const { data } = await $http.post('/documents/stenografo.QueryDocuments/', {
            organizations: [{
              id: currentRealm.id
            }],
            limit: limit,
            offset: offset
          })
          if (data.organizations[0].documents) {
            commit(types.STENOGRAFO_SET_DOCUMENTS_COUNT, data.organizations[0].count)
          }
          commit(types.STENOGRAFO_GET_DOCUMENTS_LIST_SUCCESS, data.organizations[0].documents)
        } catch (e) {
          console.log(e)
          commit(types.STENOGRAFO_GET_DOCUMENTS_LIST_FAILURE)
        }
      },
      async getDocument ({ commit, dispatch, rootGetters }, documentID) {
        commit(types.STENOGRAFO_GET_DOCUMENT_REQUEST)
        try {
          const currentRealm = rootGetters['app/currentRealm']
          const { data } = await $http.post('/documents/stenografo.GetDocuments/', {
            organizations: [{
              id: currentRealm.id,
              documents: [{
                id: documentID
              }]
            }]
          })
          commit(types.STENOGRAFO_GET_DOCUMENT_SUCCESS, data.organizations[0].documents[0])
        } catch (e) {
          console.log(e)
          commit(types.STENOGRAFO_GET_DOCUMENT_FAILURE)
        }
      },
      async updateDocument ({ commit, state, rootGetters }, documentID) {
        if (!documentID) {
          commit(types.STENOGRAFO_UPDATE_DOCUMENT_CONTENT, '')
          commit(types.STENOGRAFO_UPDATE_DOCUMENT_TITLE, '')
        }
        commit(types.STENOGRAFO_SAVE_DOCUMENT_REQUEST)
        try {
          const currentRealm = rootGetters['app/currentRealm']
          const { data } = await $http.post('/documents/stenografo.UpdateDocuments/', {
            organizations: [{
              id: currentRealm.id,
              documents: [{
                id: documentID,
                title: state.document.title,
                content: state.document.content
              }]
            }]
          })
          if (documentID === '') {
            commit(types.STENOGRAFO_UPDATE_DOCUMENT_ID, data.organizations[0].documents[0].id)
          }
          commit(types.STENOGRAFO_SAVE_DOCUMENT_SUCCESS)
        } catch (e) {
          commit(types.STENOGRAFO_SAVE_DOCUMENT_FAILURE, e)
          console.log(e)
        }
      }
    }

  }
}

export default createStenografoStore

import VDocumentEditor from '@/modules/stenografo/views/VDocumentEditor'
import VDocumentCollection from '@/modules/stenografo/views/VDocumentCollection'

export default [
  {
    path: 'd/:id',
    props: true,
    name: 'STENOGRAFO.DOCUMENT_EDITOR.$NAME',
    component: VDocumentEditor,
    require: {
      realm: true
    }
  },
  {
    path: 'list',
    name: 'STENOGRAFO.DOCUMENT_COLLECTION.$NAME',
    component: VDocumentCollection,
    require: {
      realm: true
    }
  }
]
